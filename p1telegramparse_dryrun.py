# -*- coding: utf-8 -*-
"""
Created on Wed Jan 13 15:55:52 2021

Dry run version of the P1 telegram parser. 
No MQTT broker or devices needed, the P1 message is read from file.
On execution, this file runs only once. 
If _PRODUCTION_ is set to  True, it also does a MQTT publish.

@author: Joannes //Epyon// Laveyne
"""

import json
import yaml
from datetime import datetime
from dateutil import tz
import paho.mqtt.client as mqtt
import ha_autodiscovery

_PRODUCTION_ = False
_HAdiscovery_ = False
response = []

"""
Some hardcoded vars, could be moved to the config file if needed
"""
client=mqtt.Client("utility_meter")
brokers_out={"broker1":"localhost"} 


"""
Utility function to check if meter value is numeric or string
"""
def is_number(s):
    try:
        r = float(s)
        return (True, r)
    except ValueError:
        try:
            r = int(s)
            return (True, r)
        except ValueError:
            return (False, s)

"""
Read in the config file containing the cookie configruation
and OBIS keys requested to be extracted from the meter telegram
"""
keys = {}
timestamp = 0
subs = []
with open("p1config.yaml", 'r') as stream:
    try:
        config = yaml.safe_load(stream)
        cookie = config['configuration']
        subs = [('stat/' + cookie['cookie']['id'] + '/p1/telegram',0)]
        keys = config['keys']
        try:
            if(config['configuration']['home-assistant']['auto_discovery'] == True):
                _HAdiscovery_ = True
        except: _HAdiscovery_ = False
    except yaml.YAMLError as exc:
        print(exc)
if(_PRODUCTION_):
    client.connect(brokers_out["broker1"])
    client.subscribe(subs)

"""
On receiving the MQTT payload, decode it and pass it to the telegram parser
Set the timestamp according to the payload timestamp
Perhaps add integrity check here later
"""
def on_telegram(mosq, obj, msg):
    try:
        data = json.loads(msg.payload.decode())
        telegram = data["value"].encode().decode('unicode_escape')
        timestamp = data["timestamp"]
        parseMetertelegram(telegram)
    except: print("Error parsing telegram")

"""
Parse the OBIS timestamp
"""
def parseMeterTime(mtimestamp):
    dst = True
    #OBIS timestamps have a trailing character indicating DST or not
    #Currently unused, we convert the naive timestamp to UTC based on system locale
    if(mtimestamp[-1:] == 'W' or mtimestamp[-1:] == 'S' or mtimestamp[-1:] == 'Z'):
        if(mtimestamp[-1:] == 'W'): dst = False
        mtimestamp = mtimestamp[:-1]
    try:
        #Use the system locale to turn the datetime into epoch timestamp
        datetime_object = datetime.strptime(mtimestamp, '%y%m%d%H%M%S').astimezone(tz.UTC)
        mtimestamp = int(datetime_object.timestamp())
        return mtimestamp
    except:
        return timestamp

"""
Parse the telegram, extract requested keys and publish them to the relevant MQTT topics
"""
def parseMetertelegram(telegram):
    """
    Parse the raw meter telegram into its atomic OBIS key:value pairs
    """
    dlist = []      
    for line in telegram.splitlines():
        if not line: continue  # Filter out blank lines
        splitline= line.split("(", 1)
        if len(splitline) > 1: splitline[1] = str("(") + splitline[1]
        else:
            if splitline[0][0] == '/':
                splitline = ['Header', splitline[0]]
            else: splitline = ['CRC', splitline[0]]
        dlist.append(splitline)
    meterdata = dict(dlist)
    """
    Check if the telegram contains the meter local timestamp (DSMR 4.0 or higher)
    Otherwise, use timestamp of message payload (P1 cookie local)
    """
    mtimestamp = timestamp
    if(meterdata.get('0-0:1.0.0')):
        mtimestamp = parseMeterTime(meterdata.get('0-0:1.0.0').lstrip("(").rstrip(")"))
    """
    Check if the requested OBIS keys are present in the meter telegram,
    parse them, build the JSON response and send it to the correct MQTT topic
    """
    entity = "utility_meter"
    topic = "data/devices/" + entity + "/"
    for key in keys:
        entry = keys[key]
        rvalue = meterdata.get(entry.get('key'))
        #Only build the response if the requested key is present in the telegram
        if(rvalue):
           result = {}
           result['entity'] = entity
           name = str(key)
           result['channel'] = name
           #OBIS values are (encapsulated)
           rvalue = rvalue.lstrip("(").rstrip(")")
           rvalue = rvalue.rstrip(")")
           #If no timestamp is requested to be extracted from the value, do nothing and use the default timestamp
           #Else, (try to) extract the timestamp from the OBIS value
           #These timestamps are added by some external meters (gas, heat) which are polled slower than the energy meter
           if(entry.get('timestamped') != 'no' or entry.get('timestamped') != False):   
              #Additional timestamps are also (encapsulated), try to split them off based on the separator
              if(rvalue.count(")(") == 1):
                   value = rvalue.split(')(')[1]
                   mtimestamp = parseMeterTime(rvalue.split(')(')[0])
                   rvalue = value
           #If no unit of measurement is explicitly given, try to extract the unit from the OBIS value
           unit = None
           if(entry.get('unit_of_measurement') == 'auto' or entry.get('unit_of_measurement') == None):
               #If present, the unit is preceded by an * in the OBIS value
               if(rvalue.count("*") == 1):
                   unit = rvalue.split('*')[1]
                   value = rvalue.split('*')[0]
               else: value = rvalue
           else:
               unit = str(entry.get('unit_of_measurement'))
           #Check if the OBIS value is a numeric value before doing rescaling
           if(is_number(value)[0] == True and entry.get('data_type') != 'string'):
               value = is_number(value)[1]
               #Perform value rescaling if requested
               if(entry.get('multiplier') != None):
                   if(is_number(entry.get('multiplier'))[0]):
                       value = value * is_number(entry.get('multiplier'))[1]
               if(entry.get('offset') != None):
                   if(is_number(entry.get('offset'))[0]):
                       value = value + is_number(entry.get('offset'))[0]
           result['value'] = value
           result['unit'] = unit
           if(entry.get('friendly_name') != None):
               fname = str(entry.get('friendly_name'))
               result['friendly_name'] = fname
           if(entry.get('device_class') != None):
               devclass = str(entry.get('device_class'))
               result['device_class'] = devclass
           result['timestamp'] = mtimestamp
           #Build the response for this OBIS key
           rtopic = topic + name
           resp = json.dumps(result)
           #If in production env, publish the response to the broker
           if(_PRODUCTION_):
               try:
                   client.publish(rtopic,resp)
                   #client.disconnect()
                   print(rtopic + ': ' + resp)
               except:
                   break
           #Print and temporarily store the result for debugging
           #print(rtopic + ": " + resp)
           response.append(result)
    print(response)
    global _HAdiscovery_
    if(_HAdiscovery_ == True):
        try:
            ha_autodiscovery.register(response, _PRODUCTION_)
            print("Succesfully registered device with Home Assistant")
            _HAdiscovery_ = False
        except:
            print("Error registering device with Home Assistant")
            _HAdiscovery_ = False

"""
Main testing loop
Check for meter telegram, then parse it into bits and publish them
Keeps running untill keyboard override
"""
#Nooooo you can't just import me as a module in your program nooooo
if __name__ == '__main__':
    with open('p1telegram.txt') as f:
      data = json.load(f)
      telegram = data["value"].encode().decode('unicode_escape')
      timestamp = data["timestamp"]
      parseMetertelegram(telegram)
