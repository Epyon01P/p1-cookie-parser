# -*- coding: utf-8 -*-
"""
Created on Wed Jan 13 16:05:43 2021

Minimalistic implementation of the HA MQTT autodiscovery protocol

@author: Joannes //Epyon// Laveyne
"""

import json
import sys
import paho.mqtt.client as mqtt
import time

_PRODUCTION_ = False
registration = True

client=mqtt.Client("utility_meter")
brokers_out={"broker1":"localhost"} 

uid = "utility_meter"
devname = "P1 utility meter"
model = "P1 cookie for DSMR compatible utility meters"
manufacturer = "CofyCo"
topic = "homeassistant/sensor/" + uid + "/"
state = "data/devices/" + uid +"/"

def register(keys, production):
    for key in keys:
        ctpc = topic + key['channel'] + '/config'
        msg = {}
        if(registration):
            if(key['unit'] != None): msg['unit_of_measurement'] = key['unit']
            if('device_class' in key): msg['device_class'] = key['device_class']
            msg['value_template'] = "{{ value_json.value }}"
            msg['state_topic'] = state + key['channel']
            if('friendly_name' in key): msg['name'] = key['friendly_name']
            else: msg['name'] = key['channel'].lower().replace(" ", "_")
            msg['unique_id'] = uid + '_' + key['channel'].lower().replace(" ", "_")
            dev = {}
            dev['identifiers'] = [devname.replace(" ", "_")]
            dev['name'] = devname
            dev['model'] = model
            dev['manufacturer'] = manufacturer
            msg['device'] = dev
        print(ctpc)
        print(json.dumps(msg))
        if(_PRODUCTION_ or production):
            client.connect(brokers_out["broker1"])
            client.publish(ctpc,json.dumps(msg))
            client.disconnect()
            time.sleep(0.05)
    return True

if __name__ == '__main__':
    try: 
        unregister = str(sys.argv[1])
        if (unregister) == 'true': registration = False
    except:
        pass
    keys = [{'entity': 'utility_meter', 'channel': 'header', 'value': '/FLU5«769484_A', 'unit': None, 'friendly_name': 'P1 telegram header', 'timestamp': 1610486814}, {'entity': 'utility_meter', 'channel': 'meterid', 'value': '3153414733313030313530383631', 'unit': None, 'friendly_name': 'Serial number', 'timestamp': 1610486814}, {'entity': 'utility_meter', 'channel': 'energy_consumed_t1', 'value': 592.08, 'unit': 'kWh', 'friendly_name': 'Total consumption day', 'device_class': 'energy', 'timestamp': 1610486814}, {'entity': 'utility_meter', 'channel': 'energy_consumed_t2', 'value': 401.141, 'unit': 'kWh', 'friendly_name': 'Total consumption night', 'device_class': 'energy', 'timestamp': 1610486814}, {'entity': 'utility_meter', 'channel': 'energy_injected_t1', 'value': 72.68, 'unit': 'kWh', 'friendly_name': 'Total injection day', 'device_class': 'energy', 'timestamp': 1610486814}, {'entity': 'utility_meter', 'channel': 'energy_injected_t2', 'value': 20.044, 'unit': 'kWh', 'friendly_name': 'Total injection night', 'device_class': 'energy', 'timestamp': 1610486814}, {'entity': 'utility_meter', 'channel': 'active_tariff', 'value': '0002', 'unit': None, 'friendly_name': 'Active tariff period', 'timestamp': 1610486814}, {'entity': 'utility_meter', 'channel': 'active_power_consumption', 'value': 0.173, 'unit': 'kW', 'friendly_name': 'Active power consumption', 'device_class': 'power', 'timestamp': 1610486814}, {'entity': 'utility_meter', 'channel': 'active_power_injection', 'value': '00.000', 'unit': 'kW', 'friendly_name': 'Active power injection', 'device_class': 'power', 'timestamp': 1610486814}, {'entity': 'utility_meter', 'channel': 'voltage_phase_l1', 'value': 242.6, 'unit': 'V', 'friendly_name': 'Voltage phase 1', 'device_class': 'voltage', 'timestamp': 1610486814}, {'entity': 'utility_meter', 'channel': 'voltage_phase_l2', 'value': 243.6, 'unit': 'V', 'friendly_name': 'Voltage phase 2', 'device_class': 'voltage', 'timestamp': 1610486814}, {'entity': 'utility_meter', 'channel': 'voltage_phase_l3', 'value': 243.7, 'unit': 'V', 'friendly_name': 'Voltage phase 3', 'device_class': 'voltage', 'timestamp': 1610486814}, {'entity': 'utility_meter', 'channel': 'limiter_treshold', 'value': 999.9, 'unit': 'kW', 'friendly_name': 'Maximum allowed capacity', 'device_class': 'power', 'timestamp': 1610486814}, {'entity': 'utility_meter', 'channel': 'fuse_treshold', 'value': 999.0, 'unit': 'A', 'friendly_name': 'Maximum allowed current', 'device_class': 'current', 'timestamp': 1610486814}, {'entity': 'utility_meter', 'channel': 'gas_consumption', 'value': 109.626, 'unit': 'm3', 'friendly_name': 'Total gas consumption', 'timestamp': 1610486700}]
    register(keys, _PRODUCTION_)
